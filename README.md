# Sample Tabas App

Application made as a challenge to we know each other :)

## Installation

Let's start from the beginning

```
git clone https://gitlab.com/gesianbender/tabas-challenge.git

```

install some dependencies

```
yarn install
bundle update
rails assets:precompile

```

Now let's create the databases. We're assuming here that at development environment you use MySQL with root user, without password. Fell free to change it as you like.
```
# To Develpment
tabas_challenge_development

# To Tests
tabas_challenge_test

```

Now it's time to seed some sample properties
```
rails db:seed
```


### Ok, this is it!

```rails
# Check out some tests, just  to fun
rails t

# If it's ok, run the server
rails s

```

## As you know, check out our app here
#### http://localhost:3000



## What we did
- A Rails app
- Properties and Photos as related models
- A few tests
- Seed sample data (50 proprieties)
- Three I18n locales working at frontend 
- Stored all pictures localy
- Webpack to dependencies
- Frontend similar to real Tabas
- The third photo is the cover
- Bootstraped and responsive layout
- Proprieties pagination
- Carousel to view all photos at each card

# Thanks!
