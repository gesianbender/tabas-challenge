class StaticPagesController < ApplicationController
  def home
    @pagy, @properties = pagy(Property.all)
    super
  end
end
