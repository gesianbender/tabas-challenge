class ApplicationController < ActionController::Base
    include Pagy::Backend
    include ApplicationHelper
    
    before_action :set_locale
    
    def set_locale
        I18n.locale = params[:locale] || session[:locale] || I18n.default_locale
        session[:locale] = I18n.locale
    end

    def home
        @app_name = "Sample Tabas"
    end
end
