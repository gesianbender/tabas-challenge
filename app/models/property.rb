class Property < ApplicationRecord
    has_many :photos

    validate :validate_photos
    validates :name, presence: true

    def cover
        photos.third
    end

    private

      def validate_photos
        errors.add(:photos, "too few") if photos.size < 3
      end
end
