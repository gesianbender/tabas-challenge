class Photo < ApplicationRecord
  belongs_to :property

  validates :file, presence: true
  validates :thumb, presence: true
  validates :property, presence: true
end
