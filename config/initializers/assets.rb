# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path.
# Rails.application.config.assets.paths << Emoji.images_path
# Add Yarn node_modules folder to the asset load path.
Rails.application.config.assets.paths << Rails.root.join('node_modules')

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in the app/assets
# folder are already added.
Rails.application.config.assets.precompile += %w( 
    bootstrap/dist/css/bootstrap.min.css
    jquery/dist/jquery.min.js
    bootstrap/dist/js/bootstrap.min.js
    popper.js/dist/popper.min.js
    owl.carousel/dist/owl.carousel.min.js
    owl.carousel/dist/assets/owl.carousel.min.css
    owl.carousel/dist/assets/owl.theme.default.min
 )
