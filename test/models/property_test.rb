require 'test_helper'

class PropertyTest < ActiveSupport::TestCase
  def setup
    @property = Property.new(name: "Example Property")
    @property.photos << Photo.new(file: 'a.png', thumb: 'thumb_a.png')
    @property.photos << Photo.new(file: 'b.png', thumb: 'thumb_b.png')
    @property.photos << Photo.new(file: 'c.png', thumb: 'thumb_c.png')
  end
  
  test "should be valid" do
    assert @property.valid?
  end

  test "name should be present" do
    @property.name = ""
    assert_not @property.valid?
  end

  test "should be invalid if has less than three photos" do
    @property.photos = []
    assert_not @property.valid?
  end
  
  test "should be valid if has three or more photos" do
    @property.photos << Photo.new(file: 'd.png', thumb: 'thumb_d.png')
    @property.photos << Photo.new(file: 'e.png', thumb: 'thumb_e.png')
    assert @property.valid?
  end

  test "cover must be the third photo" do
    assert_equal @property.cover.file, 'c.png'
  end

end
