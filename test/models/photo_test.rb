require 'test_helper'

class PhotoTest < ActiveSupport::TestCase
  def setup
    property = Property.new(name: "Example Property")
    @photo = Photo.new(file: 'a.png', thumb: 'thumb_a.png', property: property)
  end

  test "should be valid" do
    assert @photo.valid?
  end

  test "file should be present" do
    @photo.file = ""
    assert_not @photo.valid?
  end
  
  test "thumb should be present" do
    @photo.thumb = ""
    assert_not @photo.valid?
  end

  test "property should be present" do
    @photo.property = nil
    assert_not @photo.valid?
  end
  
end
