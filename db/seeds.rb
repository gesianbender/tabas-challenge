# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

(1..50).each do |i|
    property = Property.new(name: "Sample Property #{i}")
    property.photos << Photo.new(file: "#{i}/a.jpg", thumb: "#{i}/thumb_a.jpg")
    property.photos << Photo.new(file: "#{i}/b.jpg", thumb: "#{i}/thumb_b.jpg")
    property.photos << Photo.new(file: "#{i}/c.jpg", thumb: "#{i}/thumb_c.jpg")

    property.save
end